﻿$(document).ready(function () {
  var url = document.location.href;
  $('#searchTerm').autocomplete({
    source: url + '/PredictiveTerms'
  });
})