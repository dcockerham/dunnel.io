﻿namespace LocationSearchService.Models
{
  public class Location
  {
    public string Name { get; set; }
    public string County { get; set; }
    public double Lat { get; set; }
    public double Long { get; set; }
  }
}
