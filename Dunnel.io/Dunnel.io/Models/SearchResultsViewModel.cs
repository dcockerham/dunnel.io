﻿namespace Dunnel.io.Models
{
  using System.Collections.Generic;
  using LocationSearchService.Models;

  public class SearchResultsViewModel
  {
    public List<Location> Results { get; set; }
    public string SearchTerm { get; set; }

    public SearchResultsViewModel(List<Location> locations, string searchTerm)
    {
      Results = locations;
      SearchTerm = searchTerm;
    }
  }
}