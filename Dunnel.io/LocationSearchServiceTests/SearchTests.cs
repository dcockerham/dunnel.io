﻿namespace LocationSearchServiceTests
{
  using System.Collections.Generic;
  using LocationSearchService;
  using LocationSearchService.Interfaces;
  using LocationSearchService.Models;
  using NUnit.Framework;
  using Rhino.Mocks;

  [TestFixture]
  public class SearchTests
  {
    private List<Location> _locations;

    private const string TownOne = "Leeds";
    private const string TownTwo = "Vauxhall";
    private const string TownThree = "Wanstead";

    [SetUp]
    public void SetUp()
    {
      _locations = new LocationsTestHelper().ListOfLocations;
    }

    [Test]
    [TestCase(TownOne)]
    [TestCase(TownTwo)]
    [TestCase(TownThree)]
    public void search_should_filter_results(string testCase)
    {
      var apiMock = MockRepository.GenerateMock<IApiRequests>();
      var search = new Search(apiMock);

      apiMock.Stub(a => a.GetAllLocations()).Return(_locations);

      var results = search.SearchAllLocations(testCase);

      Assert.That(results.Count == 1);

      foreach (var location in results)
      {
        Assert.That(location.Name == testCase);
      }
    }

    [Test]
    [TestCase("ca")]
    public void predictive_terms_should_return_five_results(string testCase)
    {
      var apiMock = MockRepository.GenerateMock<IApiRequests>();
      var search = new Search(apiMock);

      apiMock.Stub(a => a.GetAllLocations()).Return(_locations);

      var results = search.GetPredictiveTerms(testCase);

      Assert.That(results.Count == 5);
    }

    [Test]
    [TestCase("ca")]
    public void predictive_terms_should_contain_search_term(string testCase)
    {
      var apiMock = MockRepository.GenerateMock<IApiRequests>();
      var search = new Search(apiMock);

      apiMock.Stub(a => a.GetAllLocations()).Return(_locations);

      var results = search.GetPredictiveTerms(testCase);

      foreach (var result in results)
      {
        Assert.That(result.ToLower().Contains(testCase));
      }
    }
  }
}
