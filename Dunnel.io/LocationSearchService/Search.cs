﻿namespace LocationSearchService
{
  using System.Collections.Generic;
  using System.Linq;
  using LocationSearchService.Interfaces;
  using Models;

  public class Search
  {
    private readonly IApiRequests _requests;

    public Search(IApiRequests requests)
    {
      _requests = requests;
    }

    public List<Location> SearchAllLocations(string searchQuery)
    {
      return FilterResults(searchQuery.ToLower(), _requests.GetAllLocations()).ToList();
    }

    private static IEnumerable<Location> FilterResults(string searchQuery, IEnumerable<Location> locations)
    {
      return locations.Where(l => l.Name.ToLower() == searchQuery);
    }

    public List<string> GetPredictiveTerms(string term)
    {
      var locations = _requests.GetAllLocations();
      var results = locations.Where(l => l.Name.ToLower().StartsWith(term.ToLower()));

      var places = new List<string>();

      foreach (var location in results.Take(5))
      {
        places.Add(location.Name);
      }

      return places;
    }
  }
}
