﻿namespace Dunnel.io.LocationAPI.Interfaces
{
  using Dunnel.io.LocationAPI.Models;

  interface IDataBaseAccess
  {
    LocationsObject GetAllLocations();
    void CacheResults<T>(T results);
  }
}