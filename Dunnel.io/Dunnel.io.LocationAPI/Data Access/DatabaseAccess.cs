﻿namespace Dunnel.io.LocationAPI
{
  using System;
  using System.Collections.Generic;
  using System.Configuration;
  using System.Data;
  using System.Data.SqlClient;
  using System.Runtime.Caching;
  using Dunnel.io.LocationAPI.Interfaces;
  using Dunnel.io.LocationAPI.Models;

  public class DatabaseAccess : IDataBaseAccess
  {
    public LocationsObject GetAllLocations()
    {
      var locations = new List<Location>();

      if (MemoryCache.Default["all_locations"] as LocationsObject == null)
      {
        var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["LocationDatabase"].ToString());

        using(connection)
        {
          try
          {
            var command = connection.CreateCommand();
            command.CommandText = StoredProcedures.GetAll;
            command.CommandType = CommandType.StoredProcedure;

            connection.Open();

            var reader = command.ExecuteReader();
            while (reader.Read())
            {
              locations.Add(new Location
              {
                County = reader[3].ToString(),
                Latitude = double.Parse(reader[6].ToString()),
                Longitude = double.Parse(reader[7].ToString()),
                Name = reader[0].ToString()
              });
            }
          }
          catch (Exception e)
          {
            return null;
          }

          CacheResults(new LocationsObject(locations));

          connection.Close();
        }
      }

      return MemoryCache.Default["all_locations"] as LocationsObject;
    }

    public void CacheResults<T>(T results)
    {
      MemoryCache.Default["all_locations"] = results;
    }
  }
}