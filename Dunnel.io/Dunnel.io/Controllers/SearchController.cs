﻿namespace Dunnel.io.Controllers
{
  using System.Web.Mvc;
  using Dunnel.io.Helpers;
  using Dunnel.io.Models;
  using LocationSearchService;

  public class SearchController : Controller
  {
    [SimpleMembership]
    public ActionResult Index()
    {
      return View();
    }

    public ActionResult Search(string searchTerm)
    {
      var service = new Search(new ApiRequests());
      var results = service.SearchAllLocations(searchTerm);
      var viewModel = new SearchResultsViewModel(results, searchTerm);

      return View("Results", viewModel);
    }

    public JsonResult PredictiveTerms(string term)
    {
      var service = new Search(new ApiRequests());
      var results = service.GetPredictiveTerms(term);

      return Json(results, JsonRequestBehavior.AllowGet);
    }
  }
}