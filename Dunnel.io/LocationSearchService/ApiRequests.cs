﻿namespace LocationSearchService
{
  using System;
  using System.Collections.Generic;
  using System.Net.Http;
  using LocationSearchService.Interfaces;
  using LocationSearchService.Models;
  using Newtonsoft.Json;

  public class ApiRequests : IApiRequests
  {
    public List<Location> GetAllLocations()
    {
      var baseAddress = new Uri("http://dunnelapi.azurewebsites.net/Locations");
      var httpClient = new HttpClient { BaseAddress = baseAddress };
      var response = httpClient.GetAsync("locations").Result.Content.ReadAsStringAsync();
      var formattedResults = JsonConvert.DeserializeObject<LocationResults>(response.Result).Locations;

      return formattedResults;
    }
  }
}
