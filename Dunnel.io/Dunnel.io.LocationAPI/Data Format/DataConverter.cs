﻿namespace Dunnel.io.LocationAPI
{
  using Dunnel.io.LocationAPI.Models;
  using Newtonsoft.Json;

  public static class DataConverter
  {
    public static string ConvertToJson<T>(T objectToConvert) 
      where T : LocationsObject
    {
      var jsonObject = JsonConvert.SerializeObject(objectToConvert);

      return jsonObject;
    }
  }
}