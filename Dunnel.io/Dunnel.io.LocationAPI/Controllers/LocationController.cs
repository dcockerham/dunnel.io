﻿namespace Dunnel.io.LocationAPI.Controllers
{
  using System.Net;
  using System.Net.Http;
  using System.Text;
  using System.Web.Http;
  using Dunnel.io.LocationAPI.Routes;

  public class LocationController : ApiController
  {
    // Gets all locations
    [System.Web.Mvc.HttpGet]
    [Route(Routes.GetAllLocations)]
    public HttpResponseMessage GetAllLocations()
    {
      var access = new DatabaseAccess();
      var locationsJson = DataConverter.ConvertToJson(access.GetAllLocations());
      var response = Request.CreateResponse(HttpStatusCode.OK);

      response.Content = new StringContent(locationsJson, Encoding.UTF8, "application/json");

      return response;
    }
  }
}