﻿namespace LocationSearchService.Interfaces
{
  using System.Collections.Generic;
  using LocationSearchService.Models;

  public interface IApiRequests
  {
    List<Location> GetAllLocations();
  }
}
