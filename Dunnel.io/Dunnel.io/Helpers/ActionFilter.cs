﻿namespace Dunnel.io.Helpers
{
  using System.Web.Mvc;

  public class SimpleMembershipAttribute : ActionFilterAttribute
  {
    public override void OnActionExecuting(ActionExecutingContext filterContext)
    {
      if (filterContext.HttpContext.Session != null && (filterContext.HttpContext.Session["dunnel-auth"] == null ||
                                                        filterContext.HttpContext.Session["dunnel-auth"].ToString() != "success"))
      {
        if (filterContext.HttpContext.Request.Url != null)
        {
          var redirectOnSuccess = filterContext.HttpContext.Request.Url.AbsolutePath;

          var redirectUrl = string.Format("?ReturnUrl={0}", redirectOnSuccess);
          var loginUrl = "/Home/SignIn" + redirectUrl;
          filterContext.HttpContext.Response.Redirect(loginUrl, true);
        }
      }
    }
  }
}