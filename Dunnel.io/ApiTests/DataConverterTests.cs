﻿namespace ApiTests
{
  using System.Collections.Generic;
  using Dunnel.io.LocationAPI;
  using Dunnel.io.LocationAPI.Models;
  using NUnit.Framework;

  [TestFixture]
  public class DataConverterTests
  {
    #region setup of tests

    private LocationsObject _locationsObject;
    private List<Location> _locations;

    private readonly Location _firstLocation = new Location
    {
      County = "thisisacounty",
      Latitude = 1.23,
      Longitude = 3.21,
      Name = "thisisaname"
    };

    private readonly Location _secondLocation = new Location
    {
      County = "thisisacountyalso",
      Latitude = 1.234,
      Longitude = 43.21,
      Name = "thisisanamealso"
    };

    [SetUp]
    public void SetUp()
    {
      _locations = new List<Location>
      {
        _firstLocation,
        _secondLocation
      };

      _locationsObject = new LocationsObject(_locations);
    }

    #endregion

    [Test]
    public void dataconverter_should_add_location_name_to_json_object()
    {
      var result = DataConverter.ConvertToJson(_locationsObject);

      Assert.That(result, Is.Not.Null);
      Assert.That(result, Is.TypeOf<string>());
      Assert.That(result.Contains(_firstLocation.Name));
    }

    [Test]
    public void dataconverter_should_add_location_county_to_json_object()
    {
      var result = DataConverter.ConvertToJson(_locationsObject);

      Assert.That(result, Is.Not.Null);
      Assert.That(result, Is.TypeOf<string>());
      Assert.That(result.Contains(_firstLocation.County));
    }

    [Test]
    public void dataconverter_should_add_location_lat_to_json_object()
    {
      var result = DataConverter.ConvertToJson(_locationsObject);

      Assert.That(result, Is.Not.Null);
      Assert.That(result, Is.TypeOf<string>());
      Assert.That(result.Contains(_secondLocation.Latitude.ToString()));
    }

    [Test]
    public void dataconverter_should_add_location_long_to_json_object()
    {
      var result = DataConverter.ConvertToJson(_locationsObject);

      Assert.That(result, Is.Not.Null);
      Assert.That(result, Is.TypeOf<string>());
      Assert.That(result.Contains(_secondLocation.Longitude.ToString()));
    }
  }
}
