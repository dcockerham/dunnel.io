﻿namespace LocationSearchServiceTests
{
  using System.Collections.Generic;
  using LocationSearchService.Models;

  class LocationsTestHelper
  {
    public List<Location> ListOfLocations = new List<Location>();

    private readonly Location _firstLocation = new Location{County = null, Lat = 0, Long = 0, Name = "Leeds"};
    private readonly Location _secondLocation = new Location{County = null, Lat = 0, Long = 0, Name = "Vauxhall"};
    private readonly Location _thirdLocation = new Location { County = null, Lat = 0, Long = 0, Name = "Wanstead" };
    private readonly Location _fourthLocation = new Location { County = null, Lat = 0, Long = 0, Name = "Ca" };
    private readonly Location _fifthLocation = new Location { County = null, Lat = 0, Long = 0, Name = "Cas" };
    private readonly Location _sixthLocation = new Location { County = null, Lat = 0, Long = 0, Name = "Cast" };
    private readonly Location _seventhLocation = new Location { County = null, Lat = 0, Long = 0, Name = "Castle" };
    private readonly Location _eigthLocation = new Location { County = null, Lat = 0, Long = 0, Name = "Castlef" };

    public LocationsTestHelper()
    {
      ListOfLocations.Add(_firstLocation);
      ListOfLocations.Add(_secondLocation);
      ListOfLocations.Add(_thirdLocation);
      ListOfLocations.Add(_fourthLocation);
      ListOfLocations.Add(_fifthLocation);
      ListOfLocations.Add(_sixthLocation);
      ListOfLocations.Add(_seventhLocation);
      ListOfLocations.Add(_eigthLocation);
    }
  }
}
