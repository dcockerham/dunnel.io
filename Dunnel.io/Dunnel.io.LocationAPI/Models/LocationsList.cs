﻿namespace Dunnel.io.LocationAPI.Models
{
  using System.Collections.Generic;

  public class LocationsObject
  {
    public List<Location> Locations { get; set; }

    public LocationsObject(List<Location> locations)
    {
      Locations = locations;
    }
  }

  public class Location
  {
    public string Name { get; set; }
    public double Longitude { get; set; }
    public double Latitude { get; set; }
    public string County { get; set; }
  }
}