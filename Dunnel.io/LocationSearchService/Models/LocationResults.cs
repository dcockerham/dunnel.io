﻿namespace LocationSearchService.Models
{
  using System.Collections.Generic;

  public class LocationResults
  {
    public List<Location> Locations { get; set; } 
  }
}
