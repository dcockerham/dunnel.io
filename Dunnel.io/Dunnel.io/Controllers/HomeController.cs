﻿namespace Dunnel.io.Controllers
{
  using System.Configuration;
  using System.Web.Mvc;
  using Helpers;

  public class HomeController : Controller
  {
    [SimpleMembership]
    public ActionResult Index()
    {
      return View();
    }

    public ActionResult SignIn()
    {
      return View();
    }

    [HttpPost]
    public ActionResult SignIn(string password)
    {
      if (password == ConfigurationManager.AppSettings["login-pwd"])
      {
        Session["dunnel-auth"] = "success";
        return RedirectToAction("Index");
      }
      return View();
    }
  }
}